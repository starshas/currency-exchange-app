package com.starshas.currencyexchange.ui.main.Others

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.starshas.currencyexchange.ui.main.Model.Currencies


class MutableLiveDataSharedPreferencesCurrencies<T : Currencies>(
    application: Application,
    private val key: String) : MutableLiveData<Currencies>() {

    var sharedPreferences: SharedPreferences

    init {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)
    }

    override fun getValue(): Currencies? {
        if (super.getValue() == null) {
            return stringToType(sharedPreferences.getString(key, null))
        }
        return super.getValue()
    }

    override fun setValue(value: Currencies?) {
        sharedPreferences.edit().putString(key, typeToString(value)).commit()
        super.setValue(value)
    }

    fun stringToType(dataJson: String?): Currencies? {
        if (dataJson == null) {
            return null
        }
        val result = Gson().fromJson(dataJson, Currencies::class.java)
        return result
    }

    fun typeToString(value: Currencies?): String? {
        if (value == null) {
            return null
        }
        val result = Gson().toJson(value, Currencies::class.java)
        return result
    }

    override fun postValue(value: Currencies?) {
        super.postValue(value)
    }

    override fun onActive() {
        super.onActive()
        value = stringToType(sharedPreferences.getString(key, null))
    }

    override fun onInactive() {
        super.onInactive()
    }
}