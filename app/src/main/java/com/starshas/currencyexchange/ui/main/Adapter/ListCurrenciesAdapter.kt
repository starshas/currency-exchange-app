package com.starshas.currencyexchange.ui.main.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.starshas.currencyexchange.R
import com.starshas.currencyexchange.ui.main.Model.Currencies

class ListCurrenciesAdapter(
    private val currencies: Currencies,
    val clickListener: (position: Int) -> Unit,
    val onItemLongClickListener: (Int) -> Boolean
): RecyclerView.Adapter<ListCurrenciesAdapter.MyViewHolder>() {

    companion object {
        const val ID_VIEW_CURRENCY_CODE = R.id.textViewCurrencyCode
        const val ID_VIEW_CURRENCY_VALUE = R.id.textViewCurrencyValue
    }
    val CURRENCIES_FIRST_ELEMENT = 0

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val textViewCurrencyValue: TextView = itemView.findViewById(ID_VIEW_CURRENCY_VALUE)
        val textViewCurrencyName: TextView = itemView.findViewById(R.id.textViewCurrencyDate)
        val textViewCurrencyCode: TextView = itemView.findViewById(ID_VIEW_CURRENCY_CODE)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_currency, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return currencies[0].rates.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val listItem = currencies[CURRENCIES_FIRST_ELEMENT].rates[position]

        holder.itemView.setOnClickListener { clickListener(position) }
        holder.itemView.setOnLongClickListener { onItemLongClickListener(position) }

        holder.textViewCurrencyCode.text = listItem.code
        holder.textViewCurrencyValue.text = listItem.mid.toString()
        holder.textViewCurrencyName.text = listItem.currency.capitalize()
    }
}