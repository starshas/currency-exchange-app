package com.starshas.currencyexchange.ui.main.Others

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.MutableLiveData
import com.starshas.currencyexchange.R
import com.starshas.currencyexchange.ui.main.Model.Currencies
import com.starshas.currencyexchange.ui.main.Retrofit.RetrofitClient
import com.starshas.currencyexchange.ui.main.RetrofitServices.RetrofitServices


object CommonClass {
    const val CURRENCY_NAME_USD = "USD"
    const val CURRENCY_NAME_PLN = "PLN"

    private val BASE_URL = "https://api.nbp.pl/"
    val retrofitService: RetrofitServices
        get() = RetrofitClient.getClient(BASE_URL).create(RetrofitServices::class.java)

    const val FAVOURITE_CURRENCY_SEPARATOR = ";"

    fun toast(context: Context, text: String?) {
        text?.let {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        }
    }

    fun getCurrencyValueByName(currencies: MutableLiveData<Currencies>, code: String): String? {
        if (currencies.value?.get(0)?.rates != null) {
            for (item in currencies.value?.get(0)!!.rates) {
                if (item.code == code) {
                    return item.mid.toString()
                }
            }
        }

        return null
    }

    fun addInterestedCurrencyOrDeleteIt(context: Context, code: String) : Boolean {
        var resultIsAdded = false

        val sharedDefaultPreferences = getSharedDefaultPreferences(context)
        var listInterestedCurrencies = sharedDefaultPreferences.getString(
            context.getString(R.string.pref_list_interested_currencies),
            null
        )

        if (listInterestedCurrencies == null) {
            listInterestedCurrencies = ""
        }

        if (listInterestedCurrencies.contains(code + FAVOURITE_CURRENCY_SEPARATOR)) {
            resultIsAdded = false
            val valueWithRemovedCode = listInterestedCurrencies.replace(code + FAVOURITE_CURRENCY_SEPARATOR, "")
            sharedDefaultPreferences.edit().putString(context.getString(R.string.pref_list_interested_currencies), valueWithRemovedCode).commit()
            return resultIsAdded
        }

        listInterestedCurrencies += code + FAVOURITE_CURRENCY_SEPARATOR
        sharedDefaultPreferences.edit().putString(context.getString(R.string.pref_list_interested_currencies), listInterestedCurrencies).commit()
        resultIsAdded = true

        return resultIsAdded
    }

    fun getListInterestedCurrencies(context: Context) : String? {
        val sharedDefaultPreferences = getSharedDefaultPreferences(context)
        var listInterestedCurrencies = sharedDefaultPreferences.getString(
            context.getString(R.string.pref_list_interested_currencies),
            null
        )

        return listInterestedCurrencies
    }

    private fun getSharedDefaultPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }
}