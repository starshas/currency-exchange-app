package com.starshas.currencyexchange.ui.main.Fragments

import android.app.Dialog
import android.os.Bundle
import android.os.Vibrator
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.starshas.currencyexchange.R
import com.starshas.currencyexchange.ui.main.Activities.MainActivity
import com.starshas.currencyexchange.ui.main.Adapter.ListCurrenciesAdapter
import com.starshas.currencyexchange.ui.main.Model.Currencies
import com.starshas.currencyexchange.ui.main.Others.CommonClass
import com.starshas.currencyexchange.ui.main.ViewModels.MainViewModel


class MainFragment : Fragment() {

    private lateinit var buttonSwapCurrencies: ImageButton
    private lateinit var recyclerView: RecyclerView
    private lateinit var buttonCurrencyConvertFrom: Button
    private lateinit var buttonCurrencyConvertTo: Button
    private lateinit var editTextConvertFrom: EditText
    private lateinit var editTextConvertTo: EditText
    private lateinit var adapterRecyclerView : ListCurrenciesAdapter

    companion object {
        fun newInstance() =
            MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        recyclerView = view.findViewById(R.id.recyclerView)
        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            LinearLayoutManager.VERTICAL
        )
        recyclerView.addItemDecoration(dividerItemDecoration)

        view.findViewById<Button>(R.id.buttonCurrenciesAll).setOnClickListener {
            viewModel.recyclerViewContentType = MainViewModel.Companion.TypeRecyclerViewContent.ALL
            loadAllCurrenciesList(viewModel.currencies.value)
        }

        view.findViewById<Button>(R.id.buttonCurrenciesInterested).setOnClickListener {
            viewModel.recyclerViewContentType = MainViewModel.Companion.TypeRecyclerViewContent.INTERESTED
            loadInterestedCurrenciesList()
        }

        if (viewModel.recyclerViewContentType == MainViewModel.Companion.TypeRecyclerViewContent.INTERESTED) {
            loadInterestedCurrenciesList()
        }

        viewModel.currencies.observe(requireActivity(),
            Observer<Currencies> { data ->
                loadAllCurrenciesList(data)
            })

        buttonCurrencyConvertFrom = view.findViewById(R.id.buttonCurrencyConvertFrom)
        buttonCurrencyConvertTo = view.findViewById(R.id.buttonCurrencyConvertTo)

        viewModel.currencyConvertFrom.observe(requireActivity(),
            Observer<String> { data ->
                buttonCurrencyConvertFrom.setText(data)
            })
        viewModel.currencyConvertTo.observe(requireActivity(),
            Observer<String> { data ->
                buttonCurrencyConvertTo.setText(data)
            })

        editTextConvertFrom = view.findViewById(R.id.editTextConvertFrom)
        editTextConvertTo = view.findViewById(R.id.editTextConvertTo)
        buttonSwapCurrencies = view.findViewById(R.id.buttonSwapCurrencies)

        buttonSwapCurrencies.setOnClickListener {
            viewModel.swapConvertingCurrencies()
            viewModel.updateConversionResult(editTextConvertFrom.text.toString())
        }

        buttonCurrencyConvertFrom.setOnClickListener {
            showDialogSelectCurrency(it as Button)
        }

        buttonCurrencyConvertTo.setOnClickListener {
            showDialogSelectCurrency(it as Button)
        }

        viewModel.textFieldOutput.observe(requireActivity(), Observer<String> { data ->
            editTextConvertTo.setText(data)
        })

        editTextConvertFrom.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(enteredValue: Editable?) {
                viewModel.updateConversionResult(enteredValue!!.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        super.onViewCreated(view, savedInstanceState)
    }

    private fun loadAllCurrenciesList(data: Currencies?) {
        if (data != null && viewModel.recyclerViewContentType == MainViewModel.Companion.TypeRecyclerViewContent.ALL) {
            val onItemClickListener = { selectedItemPosition: Int ->
                val codeSelectedCurrency = data[0].rates[selectedItemPosition].code
                (requireActivity() as MainActivity).openFragmentSelectedCurrency(
                    codeSelectedCurrency
                )
            }
            val onItemLongClickListener = { selectedItemPosition: Int ->
                val clickedItem = data[0].rates[selectedItemPosition]
                val isItemAddedToInterested =
                    CommonClass.addInterestedCurrencyOrDeleteIt(requireContext(), clickedItem.code)

                if (isItemAddedToInterested) {
                    CommonClass.toast(requireContext(), requireActivity().getString(R.string.added))
                } else {
                    CommonClass.toast(requireContext(), requireActivity().getString(R.string.removed))
                }
                true
            }

            adapterRecyclerView = ListCurrenciesAdapter(
                data,
                onItemClickListener,
                onItemLongClickListener
            )
            //adapterRecyclerView.notifyDataSetChanged()
            recyclerView.adapter = adapterRecyclerView
        }
    }

    private fun loadInterestedCurrenciesList() {
        if (viewModel.getListInterestedCurrencies().value == null) {
            val clickListener = { selectedItemPosition: Int ->
                val codeSelectedCurrency = viewModel.getListInterestedCurrencies().value!![0].rates[selectedItemPosition].code
                (requireActivity() as MainActivity).openFragmentSelectedCurrency(
                    codeSelectedCurrency
                )
            }
            val longClickListener = { selectedItemPosition: Int -> true}
            adapterRecyclerView = ListCurrenciesAdapter(
                Currencies(),
                clickListener,
                longClickListener
            )
            adapterRecyclerView.notifyDataSetChanged()
            recyclerView.adapter = adapterRecyclerView
        } else {
            val clickListener = { selectedItemPosition: Int ->
                val codeSelectedCurrency = viewModel.getListInterestedCurrencies().value!![0].rates[selectedItemPosition].code
                (requireActivity() as MainActivity).openFragmentSelectedCurrency(
                    codeSelectedCurrency
                )
            }
            val longClickListener = { selectedItemPosition: Int ->
                val isItemAddedToInterested = CommonClass.addInterestedCurrencyOrDeleteIt(requireContext(), viewModel.getListInterestedCurrencies().value!![0].rates[selectedItemPosition].code)
                if (isItemAddedToInterested) {
                    CommonClass.toast(requireContext(), requireActivity().getString(R.string.added))
                } else {
                    CommonClass.toast(requireContext(), requireActivity().getString(R.string.removed))
                }
                loadInterestedCurrenciesList()
                true
            }
            adapterRecyclerView = ListCurrenciesAdapter(
                viewModel.getListInterestedCurrencies().value!!,
                clickListener,
                longClickListener
            )
            adapterRecyclerView.notifyDataSetChanged()
            recyclerView.adapter = adapterRecyclerView
        }
    }

    private fun showDialogSelectCurrency(clickedButton: Button) {
        var dialog = Dialog(requireActivity())
        dialog.setContentView(R.layout.dialog_select_currency)
        val layout = View.inflate(requireContext(), R.layout.dialog_select_currency, null)
        val recyclerView = layout.findViewById<RecyclerView>(R.id.recyclerView)
        val currencies = viewModel.currencies.value
        if (currencies != null) {
            val onItemClickListener = { selectedItemPosition: Int ->
                val selectedItem = currencies[0]
                //CommonClass.toast(requireContext(), selectedItemPosition.toString())
                val rateCurrent = selectedItem.rates[selectedItemPosition]
                clickedButton.text = rateCurrent.code

                when (clickedButton.id) {
                    R.id.buttonCurrencyConvertFrom -> viewModel.currencySelectedConvertRateFrom.value =
                        rateCurrent.mid.toString()
                    R.id.buttonCurrencyConvertTo -> viewModel.currencySelectedConvertRateTo.value =
                        rateCurrent.mid.toString()
                }

                viewModel.currencyConvertFrom?.value = buttonCurrencyConvertFrom.text.toString()
                viewModel.currencyConvertTo?.value = buttonCurrencyConvertTo.text.toString()

                viewModel.updateConversionResult(editTextConvertFrom.text.toString())
                dialog.cancel()
            }

            val onItemLongClickListener = { selectedItemPosition: Int ->
                true
            }

            adapterRecyclerView = ListCurrenciesAdapter(currencies, onItemClickListener, onItemLongClickListener)
            recyclerView.adapter = adapterRecyclerView
            //adapter.notifyDataSetChanged()

            dialog.setContentView(layout)
            dialog.show()
            val window = dialog.getWindow()
            window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        } else {
            CommonClass.toast(requireContext(), "List of currencies is not loaded")
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.updateListCurrencies()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }
}