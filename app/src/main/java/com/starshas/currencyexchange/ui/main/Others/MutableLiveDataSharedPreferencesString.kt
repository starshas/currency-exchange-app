package com.starshas.currencyexchange.ui.main.Others

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.starshas.currencyexchange.ui.main.Model.Currencies


class MutableLiveDataSharedPreferencesString<T : String>(
    application: Application,
    private val key: String) : MutableLiveData<String>() {

    var sharedPreferences: SharedPreferences

    init {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)
    }

    override fun getValue(): String? {
        if (super.getValue() == null) {
            return sharedPreferences.getString(key, null)
        }
        return super.getValue()
    }

    override fun postValue(value: String?) {
        super.postValue(value)
    }

    override fun onActive() {
        super.onActive()
        value = sharedPreferences.getString(key, null)
    }

    override fun onInactive() {
        super.onInactive()
    }
}