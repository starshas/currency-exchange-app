package com.starshas.currencyexchange.ui.main.ViewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.starshas.currencyexchange.ui.main.Model.Currencies
import com.starshas.currencyexchange.ui.main.Model.CurrenciesItem
import com.starshas.currencyexchange.ui.main.Model.CurrencyHistory
import com.starshas.currencyexchange.ui.main.Model.Rate
import com.starshas.currencyexchange.ui.main.Others.CommonClass
import com.starshas.currencyexchange.ui.main.Others.MutableLiveDataSharedPreferencesCurrencies
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val listInterestedCurrencies: MutableLiveDataSharedPreferencesCurrencies<Currencies> = MutableLiveDataSharedPreferencesCurrencies(application, "interested_currencies")

    var recyclerViewContentType: TypeRecyclerViewContent = TypeRecyclerViewContent.ALL

    var currencySelectedConvertRateFrom: MutableLiveData<String> = MutableLiveData()
    var currencySelectedConvertRateTo: MutableLiveData<String> = MutableLiveData()
    var currencyConvertFrom: MutableLiveData<String> = MutableLiveData()
    var currencyConvertTo: MutableLiveData<String> = MutableLiveData()
    var textFieldOutput: MutableLiveData<String> = MutableLiveData()

    var currencies: MutableLiveDataSharedPreferencesCurrencies<Currencies> = MutableLiveDataSharedPreferencesCurrencies(application, "currencies")
    val currencyHistory: MutableLiveData<CurrencyHistory> = MutableLiveData()

    init {
        if (currencies.value != null) {
            currencySelectedConvertRateTo.value = CommonClass.getCurrencyValueByName(currencies, CommonClass.CURRENCY_NAME_USD)
        }

        if (currencySelectedConvertRateFrom.value == null) {
            currencySelectedConvertRateFrom.value = 1.toString()
        }

        currencyConvertFrom.value = CommonClass.CURRENCY_NAME_PLN
        currencyConvertTo.value = CommonClass.CURRENCY_NAME_USD
    }

    fun updateListCurrencies() {
        var list = CommonClass.retrofitService.getCurrenciesList()
        list.enqueue(object : Callback<Currencies> {
            override fun onFailure(call: Call<Currencies>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<Currencies>,
                response: Response<Currencies>
            ) {
                val body: Currencies = response.body() as Currencies
                currencies.setValue(body)
                if (currencySelectedConvertRateTo.value == null) {
                    currencySelectedConvertRateTo.value = CommonClass.getCurrencyValueByName(currencies, CommonClass.CURRENCY_NAME_USD)
                }
            }

        })
    }

    fun updateHistoryForSelectedCurrency(codeSelectedCurrency: String?) {
        if (codeSelectedCurrency == null)
            return
        var list = CommonClass.retrofitService.getCurrencyHistory(codeSelectedCurrency)
        list.enqueue(object : Callback<CurrencyHistory> {
            override fun onFailure(call: Call<CurrencyHistory>, t: Throwable) {
                //System.err.println("$")
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<CurrencyHistory>,
                response: Response<CurrencyHistory>
            ) {
                val body: CurrencyHistory = response.body() as CurrencyHistory
                currencyHistory.value = body
            }

        })
    }

    fun swapConvertingCurrencies() {
        var temp = currencySelectedConvertRateFrom.value
        temp = if (temp == null) "1.0" else currencySelectedConvertRateFrom.value
        currencySelectedConvertRateFrom.value = currencySelectedConvertRateTo.value
        currencySelectedConvertRateTo.value = temp

        val temp2 = currencyConvertFrom.value
        currencyConvertFrom.value = currencyConvertTo.value
        currencyConvertTo.value = temp2
    }

    fun updateConversionResult(field1: String) {
        if (currencies != null) {
            var currencySelectedConvertRateFrom =
                currencySelectedConvertRateFrom.value?.toDoubleOrNull()
            val currencySelectedConvertRateTo =
                currencySelectedConvertRateTo.value?.toDoubleOrNull()

            if (field1
                    .toDoubleOrNull() != null && currencySelectedConvertRateFrom != null && currencySelectedConvertRateTo != null
            ) {
                if (currencySelectedConvertRateTo != 0.0) {
                    val ratio: Double =
                        currencySelectedConvertRateFrom / currencySelectedConvertRateTo
                    val result: Float = (field1.toDouble() * ratio).toFloat()
                    textFieldOutput.value = result.toString()
                }
            }
        } else {
            CommonClass.toast(getApplication(), "List of currencies is not loaded")
        }
    }

    fun clearCurrencyHistory() {
        currencyHistory.value = null
    }

    fun getListInterestedCurrencies(): MutableLiveDataSharedPreferencesCurrencies<Currencies> {
        val listInterestedCurrencies1 = CommonClass.getListInterestedCurrencies(getApplication())
        var currencies1 = Currencies()

        val ratesNew = mutableListOf<Rate>()
        currencies1.add(CurrenciesItem(currencies.value!![0].effectiveDate, currencies.value!![0].effectiveDate,
            ratesNew, currencies.value!![0].table))
        if (listInterestedCurrencies1 != null) {
            for (item in currencies.value!![0].rates)
                if (listInterestedCurrencies1.contains("${item.code};")) {
                    ratesNew.add(item)
                }
        }

        listInterestedCurrencies.value = currencies1
        return listInterestedCurrencies
    }

    companion object {
        enum class TypeRecyclerViewContent {
            ALL,
            INTERESTED
        }
    }
}