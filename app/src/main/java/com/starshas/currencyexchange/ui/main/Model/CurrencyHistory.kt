package com.starshas.currencyexchange.ui.main.Model

data class CurrencyHistory(
    val code: String,
    val currency: String,
    val rates: List<RatesHistory>,
    val table: String
)