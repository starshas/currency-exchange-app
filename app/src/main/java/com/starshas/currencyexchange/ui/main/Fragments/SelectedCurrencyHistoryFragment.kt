package com.starshas.currencyexchange.ui.main.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.starshas.currencyexchange.R
import com.starshas.currencyexchange.ui.main.Activities.MainActivity
import com.starshas.currencyexchange.ui.main.Adapter.SelectedCurrencyHistoryAdapter
import com.starshas.currencyexchange.ui.main.Model.CurrencyHistory
import com.starshas.currencyexchange.ui.main.Others.CommonClass
import com.starshas.currencyexchange.ui.main.ViewModels.MainViewModel

class SelectedCurrencyHistoryFragment : Fragment() {

    private lateinit var textViewSelectedCurrency: TextView
    private lateinit var recyclerView: RecyclerView

    companion object {
        lateinit var selectedCurrency: String

        fun newInstance(selectedCurrency: String): SelectedCurrencyHistoryFragment {
            this.selectedCurrency = selectedCurrency
            return SelectedCurrencyHistoryFragment()
        }
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_selected_currency, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView = view.findViewById(R.id.recyclerView)
        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            LinearLayoutManager.VERTICAL
        )
        recyclerView.addItemDecoration(dividerItemDecoration)

        textViewSelectedCurrency = view.findViewById(R.id.textViewSelectedCurrency)
        textViewSelectedCurrency.text = selectedCurrency

        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        viewModel.updateHistoryForSelectedCurrency(selectedCurrency)
        viewModel.currencyHistory.observe(requireActivity(),
            Observer<CurrencyHistory?>{ data ->
                if (data != null && getContext() != null) {
                    viewModel.currencySelectedConvertRateTo.value = CommonClass.getCurrencyValueByName(viewModel.currencies, "USD")

                    var adapter = SelectedCurrencyHistoryAdapter(data.rates.reversed(), null)
                    adapter.notifyDataSetChanged()
                    recyclerView.adapter = adapter
                }
            })

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        viewModel.clearCurrencyHistory()
        super.onDestroyView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

}