package com.starshas.currencyexchange.ui.main.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.starshas.currencyexchange.R
import com.starshas.currencyexchange.ui.main.Model.RatesHistory

class SelectedCurrencyHistoryAdapter(
    private val ratesHistory: List<RatesHistory>,
    val clickListener: ((position: Int) -> Unit)?
):RecyclerView.Adapter<SelectedCurrencyHistoryAdapter.MyViewHolder>() {

    companion object {
        const val ID_VIEW_CURRENCY_DATE = R.id.textViewCurrencyDate
        const val ID_VIEW_CURRENCY_VALUE = R.id.textViewCurrencyValue
    }

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val textViewCurrencyValue: TextView = itemView.findViewById(ID_VIEW_CURRENCY_VALUE)
        val textViewCurrencyName: TextView = itemView.findViewById(ID_VIEW_CURRENCY_DATE)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_history_currency, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return ratesHistory.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val listItem = ratesHistory[position]

        if (clickListener != null) {
            holder.itemView.setOnClickListener { clickListener!!(position) }
        }

        holder.textViewCurrencyValue.text = listItem.mid.toString()
        holder.textViewCurrencyName.text = listItem.effectiveDate
    }
}