package com.starshas.currencyexchange.ui.main.RetrofitServices

import com.starshas.currencyexchange.ui.main.Model.Currencies
import com.starshas.currencyexchange.ui.main.Model.CurrencyHistory
import retrofit2.Call
import retrofit2.http.*

interface RetrofitServices {
    @GET("/api/exchangerates/tables/a/")
    fun getCurrenciesList(): Call<Currencies>

    @GET("/api/exchangerates/rates/a/{currencyCode}/last/100/")
    fun getCurrencyHistory(@Path("currencyCode") currencyCode: String): Call<CurrencyHistory>
}