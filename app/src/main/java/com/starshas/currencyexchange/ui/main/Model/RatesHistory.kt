package com.starshas.currencyexchange.ui.main.Model

data class RatesHistory(
    val effectiveDate: String,
    val mid: Double,
    val no: String
)