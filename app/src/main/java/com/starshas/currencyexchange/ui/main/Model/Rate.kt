package com.starshas.currencyexchange.ui.main.Model

data class Rate(
    val code: String,
    val currency: String,
    val mid: Double
)