package com.starshas.currencyexchange.ui.main.Model

data class CurrenciesItem(
    val effectiveDate: String,
    val no: String,
    val rates: List<Rate>,
    val table: String
)