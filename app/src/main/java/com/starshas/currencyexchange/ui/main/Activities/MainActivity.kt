package com.starshas.currencyexchange.ui.main.Activities

import android.os.Bundle
import android.text.Html
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.starshas.currencyexchange.R
import com.starshas.currencyexchange.ui.main.Fragments.MainFragment
import com.starshas.currencyexchange.ui.main.Fragments.SelectedCurrencyHistoryFragment
import com.starshas.currencyexchange.ui.main.ViewModels.MainViewModel

class MainActivity : AppCompatActivity() {

    lateinit var mainFragment: MainFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            ViewModelProvider(this).get(MainViewModel::class.java)
            mainFragment = MainFragment.newInstance()
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, mainFragment)
                .commitNow()
        }
    }

    fun openFragmentSelectedCurrency(selectedCurrency: String?) {
        if (selectedCurrency == null) {
            return
        }
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, SelectedCurrencyHistoryFragment.newInstance(selectedCurrency)).addToBackStack(null)
            .commit()
    }
}